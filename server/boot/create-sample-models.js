var async = require('async');
module.exports = function(app) {
  //data sources
  var mongoDs = app.dataSources.mongoDs; // 'name' of your mongo connector, you can find it in datasource.json

  //create all models
  async.parallel({
    reviewers: async.apply(createReviewers),
    stories: async.apply(createStories)
  }, function(err, results) {
    if (err)
      throw err;
    createReviews(results.reviewers, results.stories, function(err) {
      console.log('> models created sucessfully');
    });
  });
  //create reviewers
  function createReviewers(cb) {
    mongoDs.automigrate('Reviewer', function(err) {
      if (err)
        return cb(err);
      var Reviewer = app.models.Reviewer;
      Reviewer.create([
        {
          email: 'foo@bar.com',
          password: 'foobar'
        }, {
          email: 'john@doe.com',
          password: 'johndoe'
        }, {
          email: 'jane@doe.com',
          password: 'janedoe'
        }
      ], cb);
    });
  }
  //create stories
  function createStories(cb) {
    mongoDs.automigrate('Story', function(err) {
      if (err)
        return cb(err);
      var Story = app.models.Story;
      Story.create([
        {
          title: 'Bel Cafe',
          brief: 'Vancouver',
          createdAt: Date.now()
        }, {
          title: 'Three Bees Coffee House',
          brief: 'San Mateo',
          createdAt: Date.now()
        }, {
          title: 'Caffe Artigiano',
          brief: 'Vancouver',
          createdAt: Date.now()
        }
      ], cb);
    });
  }
  //create reviews
  function createReviews(reviewers, stories, cb) {
    mongoDs.automigrate('Review', function(err) {
      if (err)
        return cb(err);
      var Review = app.models.Review;
      var DAY_IN_MILLISECONDS = 1000 * 60 * 60 * 24;
      Review.create([
        {
          date: Date.now() - (DAY_IN_MILLISECONDS * 4),
          rating: 5,
          comments: 'A very good story.',
          publisherId: reviewers[0].id,
          storyId: stories[0].id
        }, {
          date: Date.now() - (DAY_IN_MILLISECONDS * 3),
          rating: 5,
          comments: 'Quite pleasant.',
          publisherId: reviewers[1].id,
          storyId: stories[0].id
        }, {
          date: Date.now() - (DAY_IN_MILLISECONDS * 2),
          rating: 4,
          comments: 'It was ok.',
          publisherId: reviewers[1].id,
          storyId: stories[1].id
        }, {
          date: Date.now() - (DAY_IN_MILLISECONDS),
          rating: 4,
          comments: 'I go here everyday.',
          publisherId: reviewers[2].id,
          storyId: stories[2].id
        }
      ], cb);
    });
  }
};
