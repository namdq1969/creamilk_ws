'use strict';

module.exports = function(Story) {
  Story.status = function(cb) {
    cb(null, 'Sorry! No audio files.')
  };
  Story.remoteMethod(
    'status', {
      http: {
        path: '/status',
        verb: 'get'
      },
      returns: {
        arg: 'status',
        type: 'string'
      }
    }
  );
};
